/* eslint-disable @typescript-eslint/no-var-requires */
//const { roundToNearestMinutes } = require('date-fns')
const path = require('path')

const {
  name: short_name,
  description,
  title,
  homepage: siteUrl,
  author: { email: authorEmail, name: authorName, url: authorURL },
  keywords,
  repository,
  goat_counter,
  logo_path
} = require('./package.json')

const content = require('./collections').map(({ directory, name }) => ({
  resolve: `gatsby-source-filesystem`,
  options: {
    path: path.join(__dirname, `content`, directory),
    name
  }
}))

const domain = siteUrl.replace(/https?:\/\//, '')
const pathPrefix =
  '/' +
  (domain.indexOf('/') > 0
    ? domain.split('/').filter(Boolean).slice(1).join('/')
    : '')
const version =
  (process.env.GATSBY_COMMIT_HASH || 'development') +
  ' - ' +
  new Date().toLocaleString()

module.exports = {
  pathPrefix,
  siteMetadata: {
    version,
    siteUrl,
    repository,
    author: {
      email: authorEmail,
      name: authorName,
      url: authorURL
    },
    title,
    description,
    keywords
  },
  flags: {
    FAST_REFRESH: true
  },
  plugins: [
    goat_counter.id && {
      resolve: `gatsby-plugin-goatcounter`,
      options: {
        code: goat_counter.id,
        pixel: true
      }
    },
    `gatsby-plugin-emotion`,
    `gatsby-transformer-yaml`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/styles/typography.ts`
      }
    },
    {
      resolve: 'gatsby-plugin-svgr',
      options: {
        include: /assets/,
        prettier: true,
        dimensions: true,
        replaceAttrValues: { '#000': '{props.color}' },
        icon: true,
        memo: true,
        svgProps: { 'data-svgo': 'true' },
        svgo: true,
        svgoConfig: {
          plugins: [
            { removeXMLNS: true },
            { removeViewBox: true },
            { cleanupIDs: true }
          ]
        }
      }
    },
    /*
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /assets/,
          omitKeys: [
            'xmlnsDc',
            'xmlnsCc',
            'xmlnsRdf',
            'xmlnsSvg',
            'xmlnsSodipodi',
            'xmlnsInkscape'
          ]
        }
      }
    },
    */
    ...content,
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.mdx`, `.md`],
        remarkPlugins: [require('remark-images')],
        gatsbyRemarkPlugins: [
          `gatsby-remark-responsive-iframe`,
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1035,
              sizeByPixelDensity: true,
              withWebp: true
            }
          },
          {
            resolve: 'gatsby-remark-embed-video',
            options: {
              width: 800,
              ratio: 1.77,
              height: 400,
              related: false,
              noIframeBorder: true,
              urlOverrides: [
                {
                  id: 'youtube',
                  embedURL: (videoId) =>
                    `https://www.youtube-nocookie.com/embed/${videoId}`
                }
              ],
              containerClass: 'embedVideo-container'
            }
          },
          {
            resolve: `gatsby-remark-table-of-contents`,
            options: {
              exclude: 'Table of Contents',
              tight: false,
              fromHeading: 1,
              toHeading: 6,
              className: 'table-of-contents'
            }
          },
          `gatsby-remark-autolink-headers`,
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              classPrefix: `language-`,
              inlineCodeMarker: null,
              aliases: {}
            }
          }
        ]
      }
    },
    `gatsby-plugin-typescript`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-netlify-cms`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: description,
        short_name,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#525dce`,
        display: `standalone`,
        icon: logo_path
      }
    },
    {
      resolve: 'gatsby-plugin-offline',
      options: {
        workboxConfig: {
          globPatterns: [logo_path]
        }
      }
    }
  ].filter(Boolean)
}
