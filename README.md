# MDX TSX Gatsby Starter

(That's a mouthful, for sure).

This is a good basis for creating a site. It supports:

- MDX Pages
- MDX Blog Posts with pagination
- Categories
- Category pages with pagination
- Images & Galleries
- Typescript
- Extended VSCode support, with run configurations, and debugging through VSCode, Chrome/ium, and Firefox
- To avoid using Gitlab's Pipelines too much, the site only rebuilds when pushes to `master` are made.
- Commit hash in meta tags so you can know which version you're looking at
- support for [goatcounter](https://www.goatcounter.com) for privacy-minded analytics
- favicon generation
- Images get an alt description (`author-family-this-is-the-title.jpg` becomes `this is the title, by author family`). Better accessibility features are on the way
- automatic Table of Contents generation. Use a codeblock with `toc` as the language to place it (see [here](https://www.gatsbyjs.com/plugins/gatsby-remark-table-of-contents/) for more information)
- Video embedding with "\`video: \[VideoTitle\]\(https://www.youtube.com/watch?v=dQw4w9WgXcQ\)\`"

Coming up:

- Admin CMS through Netlify
- Server-less Gitlab integration (authenticate through Gitlab in your browser)
- Comments through staticman

Find this repo in action on [https://xananax.gitlab.io/gatsby-mdx-tsx-starter](https://xananax.gitlab.io/gatsby-mdx-tsx-starter)

## How to Set Up

1. Fork
2. Invite [StaticManLab](https://gitlab.com/staticmanlab) or [Framasoft's StaticManLab](https://framagit.org/staticmanlab1) as a developper. You can also host your own, read more about it here: 
  - [how to use staticman on gitlab? · Issue #293 · eduardoboucas/staticman](https://github.com/eduardoboucas/staticman/issues/293)
  - [Config GitLab Repo for Staticman](https://vincenttam.gitlab.io/post/2018-09-16-staticman-powered-gitlab-pages/1/#existing-solution-github-mirror-to-gitlab)
3. Edit `package.json` and replace all relevant values there
4. If you intend to use Netlify CMS, also edit `static/config.yml`. This should be auto-generated in future versions

## Development

Run dev mode:
```sh
npm start
```

Clean, then run dev mode:
```sh
npm start:clean
```

Prettify all files:
```sh
npm run pretty
```

# Design inspirations:

- [Philip Hugle](https://philiphugle.de/en/)
- [Minimal One Page Websites (Page 6 of 92)](https://onepagelove.com/tag/minimal/page/6)
- [Fuck I Wish I Knew That](https://fuckiwishiknewth.at/)
- [DUEZEROUNO](https://duezero.uno/)
- [Arnaud Alves — Independent​ Web & Product Designer](https://arnaudalves.fr/)
- [Glitch Theme Demo – Just another BrutalistThemes.com Demos site](https://demo.brutalistthemes.com/glitch/)
- [Barebone Theme Demo – Just another BrutalistThemes.com Demos site](https://demo.brutalistthemes.com/barebone/)
- [Jane Doe](https://themes.gohugo.io/theme/anatole/)
- [Nice Blogs | Nice Blogs](https://nostalgic-liskov-e76c32.netlify.app/)
- [Devlog - Tiled Map Editor by Thorbjørn](https://thorbjorn.itch.io/tiled/devlog)
