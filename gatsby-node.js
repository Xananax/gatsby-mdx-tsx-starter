/* eslint-disable @typescript-eslint/no-var-requires */

const path = require('path')

const COLLECTIONS = require('./collections')

const pluckCategories = (edges) =>
  Object.keys(
    edges.reduce((acc, value) => {
      value.node.fields.categories.forEach((category) => {
        if (!acc[category]) {
          acc[category] = category
        }
      })
      return acc
    }, {})
  )

const filterEdges = (name, edges) =>
  edges.filter((edge) => edge.node.fields.collection.name === name)

const toTags = (tags) => {
  if (!tags) {
    return []
  }
  if (Array.isArray(tags)) {
    return [...new Set(tags)]
  }
  if (tags.indexOf(',') >= 0) {
    return [
      ...new Set(
        tags
          .split(/,/)
          .map((s) => s.trim())
          .filter(Boolean)
      )
    ]
  }
  return [
    ...new Set(
      tags
        .split(/\s+/)
        .map((s) => s.trim())
        .filter(Boolean)
    )
  ]
}
const groupByCategory = (edges) =>
  edges.reduce((acc, value) => {
    value.node.fields.categories.forEach((category) => {
      if (!acc[category]) {
        acc[category] = []
      }
      acc[category].push(value)
    })
    return acc
  }, {})

const createCategoryPages = (
  pathPrefix,
  postsPerPage,
  component,
  createPage,
  edges
) => {
  const categories = pluckCategories(edges)

  const postsByCategory = groupByCategory(edges)

  Object.keys(postsByCategory).forEach((category) => {
    createPaginatedPages(
      component,
      postsPerPage,
      createPage,
      postsByCategory[category],
      `${pathPrefix}/categories/${category}`,
      { categories, activeCategory: category }
    )
  })
}

const createItemsPages = (_name, component, createPage, edges) => {
  edges.forEach(({ node }, i) => {
    const prev = i === 0 ? null : edges[i - 1].node
    const next = i === edges.length - 1 ? null : edges[i + 1].node
    const slug = node.fields.slug

    createPage({
      path: node.fields.slug,
      component,
      context: {
        slug,
        id: node.id,
        prev,
        next
      }
    })
  })
}

const createList = (
  _name,
  pathPrefix,
  postsPerPage,
  component,
  createPage,
  edges
) => {
  const categories = pluckCategories(edges)
  createPaginatedPages(component, postsPerPage, createPage, edges, pathPrefix, {
    categories
  })
}

const createPaginatedPages = (
  component,
  postsPerPage,
  createPage,
  edges,
  pathPrefix,
  context
) => {
  const pages = edges.reduce((acc, value, index) => {
    const pageIndex = Math.floor(index / postsPerPage)

    if (!acc[pageIndex]) {
      acc[pageIndex] = []
    }

    acc[pageIndex].push(value.node.id)

    return acc
  }, [])

  pages.forEach((page, index) => {
    const previousPagePath = `${pathPrefix}/${index + 1}`
    const nextPagePath = index === 1 ? pathPrefix : `${pathPrefix}/${index - 1}`

    createPage({
      path: index > 0 ? `${pathPrefix}/${index}` : `${pathPrefix}`,
      component,
      context: {
        pagination: {
          page,
          nextPagePath: index === 0 ? null : nextPagePath,
          previousPagePath:
            index === pages.length - 1 ? null : previousPagePath,
          pageCount: pages.length,
          pathPrefix
        },
        ...context
      }
    })
  })
}

const createCollection = (
  {
    name,
    postsPerPage = 12,
    listPageComponent,
    hasCategoryPage,
    component,
    pathPrefix = `/${name}`
  },
  createPage,
  edges
) => {
  edges = filterEdges(name, edges)
  if (!edges.length) {
    return
  }
  listPageComponent &&
    createList(
      name,
      pathPrefix,
      postsPerPage,
      listPageComponent,
      createPage,
      edges
    )
  createItemsPages(name, component, createPage, edges)
  listPageComponent &&
    hasCategoryPage &&
    createCategoryPages(
      pathPrefix,
      postsPerPage,
      listPageComponent,
      createPage,
      edges
    )
}

exports.createPages = async ({ actions: { createPage }, graphql }) => {
  const { data, errors } = await graphql(`
    query {
      allMdx(sort: { order: DESC, fields: [frontmatter___date] }) {
        edges {
          node {
            id
            excerpt(pruneLength: 250)
            fields {
              title
              slug
              categories
              collection {
                name
              }
            }
            body
          }
        }
      }
    }
  `)
  if (errors) {
    throw errors
  }

  const { edges } = data.allMdx

  COLLECTIONS.forEach((collection) => {
    createCollection(collection, createPage, edges)
  })
}

exports.onCreateWebpackConfig = ({
  actions,
  getConfig: _getConfig,
  stage: _stage
}) => {
  actions.setWebpackConfig({
    devtool: 'eval-source-map',
    resolve: {
      modules: [path.resolve(__dirname, 'src'), 'node_modules'],
      alias: {
        '@/components': path.resolve(__dirname, 'src/components')
      }
    }
  })
  // the below solves the react-hot-loader warning (see https://github.com/gatsbyjs/gatsby/issues/11934)
  // unecessary with fast-refresh
  /*
  const config = getConfig()
  if (stage.startsWith('develop') && config.resolve) {
    config.resolve.alias = {
      ...config.resolve.alias,
      'react-dom': '@hot-loader/react-dom'
    }
  }
  */
}

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions

  if (node.internal.type === `Mdx`) {
    const collection_name = getNode(node.parent).sourceInstanceName

    const slug =
      node.frontmatter.slug || path.basename(node.fileAbsolutePath, '.mdx')
    const collection = COLLECTIONS.find(({ name }) => name == collection_name)
    if (!collection) {
      return
    }
    const pathPrefix =
      'pathPrefix' in collection &&
      collection.pathPrefix !== false &&
      collection.pathPrefix !== null
        ? collection.pathPrefix
        : `/${collection_name}`
    const slug_total = `${pathPrefix}/${slug}`

    createNodeField({
      node,
      name: 'collection',
      value: collection
    })

    createNodeField({
      name: 'id',
      node,
      value: node.id
    })

    createNodeField({
      name: 'title',
      node,
      value: node.frontmatter.title
    })

    createNodeField({
      name: 'description',
      node,
      value: node.frontmatter.description || ''
    })

    createNodeField({
      name: 'slug',
      node,
      value: slug_total
    })

    createNodeField({
      name: 'date',
      node,
      value: node.frontmatter.date || ''
    })

    createNodeField({
      name: 'banner',
      node,
      banner: node.frontmatter.banner
    })

    createNodeField({
      name: 'categories',
      node,
      value: toTags(node.frontmatter.categories) || []
    })

    createNodeField({
      name: 'keywords',
      node,
      value: toTags(node.frontmatter.keywords) || []
    })
  } else if (node.internal.type === 'ImageSharp') {
    const fileNode = getNode(node.parent)
    const ext = path.extname(fileNode.relativePath)
    const filename = path.basename(fileNode.relativePath, ext)
    const dirname = path.basename(path.dirname(fileNode.relativePath))
    const [name, surname, ...title] = filename.split('-')

    createNodeField({
      node,
      name: 'author',
      value: { name: name || '', surname: surname || '' }
    })

    createNodeField({
      node,
      name: 'title',
      value: (title && title.join(' ')) || ''
    })

    createNodeField({
      node,
      name: 'album',
      value: dirname || ''
    })
  }
}

exports.onCreateBabelConfig = ({ actions }) => {
  actions.setBabelPreset({
    name: '@babel/preset-react',
    options: { runtime: 'automatic', importSource: '@emotion/react' }
  })
  actions.setBabelPlugin({ name: '@emotion/babel-plugin' })
}
