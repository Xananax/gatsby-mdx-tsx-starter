/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path')

module.exports = [
  {
    directory: 'blog',
    name: 'blog',
    postsPerPage: 2,
    listPageComponent: path.resolve(`src/templates/postList.tsx`),
    hasCategoryPage: true,
    component: path.resolve(`./src/templates/post.tsx`)
  },
  {
    directory: 'gallery',
    name: 'images'
  },
  {
    directory: 'comments',
    name: 'comments'
  },
  {
    directory: 'pages',
    name: 'pages',
    pathPrefix: '',
    component: path.resolve(`./src/templates/page.tsx`)
  },
  {
    directory: 'projects',
    name: 'projects',
    postsPerPage: 2,
    listPageComponent: path.resolve(`src/templates/projectList.tsx`),
    hasCategoryPage: true,
    component: path.resolve(`./src/templates/project.tsx`)
  }
]
