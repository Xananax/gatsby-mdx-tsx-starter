declare module '*.png' {
  const content: string
  export default content
}
declare module '*.svg' {
  const content: string
  export default content
}

declare module '*.mdx' {
  let MDXComponent: (props: any) => JSX.Element
  export default MDXComponent
}

declare module '@mdx-js/react' {
  import * as React from 'react'
  type ComponentType =
    | 'a'
    | 'blockquote'
    | 'code'
    | 'del'
    | 'em'
    | 'h1'
    | 'h2'
    | 'h3'
    | 'h4'
    | 'h5'
    | 'h6'
    | 'hr'
    | 'img'
    | 'inlineCode'
    | 'li'
    | 'ol'
    | 'p'
    | 'pre'
    | 'strong'
    | 'sup'
    | 'table'
    | 'td'
    | 'thematicBreak'
    | 'tr'
    | 'ul'
  export type Components = {
    [key in ComponentType]?: React.ComponentType<any>
  }
  export interface MDXProviderProps {
    children: React.ReactNode
    components?: Components
  }
  export class MDXProvider extends React.Component<MDXProviderProps> {}
}

interface SiteQueryResult {
  siteMetadata: {
    version: string
    siteUrl: string
    repository: string
    title: string
    description: string
    author: {
      email: string
      name: string
      url: string
    }
    keywords: string[]
  }
}

interface GatsbyImagePropsBase {
  id: string
  fadeIn?: boolean
  durationFadeIn?: number
  title?: string
  alt?: string
  className?: string | Record<string, unknown>
  critical?: boolean
  crossOrigin?: string | boolean
  style?: Record<string, unknown>
  imgStyle?: Record<string, unknown>
  placeholderStyle?: Record<string, unknown>
  placeholderClassName?: string
  backgroundColor?: string | boolean
  onLoad?: () => void
  onError?: (_event: any) => void
  onStartLoad?: (_param: { wasCached: boolean }) => void
  Tag?: string
  itemProp?: string
  loading?: `auto` | `lazy` | `eager`
  draggable?: boolean
  fields: {
    album: string
    author: {
      name: string
      surname: string
    }
    title: string
  }
}

interface FrontMatter<GatsbyImageType extends GatsbyImagePropsBase> {
  description: string
  title: string
  date: string
  banner: {
    childImageSharp: GatsbyImageType
  }
}

interface MDXDoc<GatsbyImageType extends GatsbyImagePropsBase> {
  excerpt: string
  id: string
  fields: {
    slug: string
    categories: string[]
    keywords: string[]
  }
  frontmatter: FrontMatter<GatsbyImageType>
  body: string
}

interface GatsbyImagePropsFluid extends GatsbyImagePropsBase {
  fluid?: FluidObject | FluidObject[]
}

interface GatsbyImagePropsFixed extends GatsbyImagePropsBase {
  resolutions?: FixedObject
  fixed?: FixedObject | FixedObject[]
}

type GatsbyImageProps = GatsbyImagePropsFixed | GatsbyImagePropsFluid

interface MDXDocList<GatsbyImageType extends GatsbyImagePropsBase> {
  edges: {
    node: MDXDoc<GatsbyImageType>
  }[]
}

interface Pagination {
  page: string[]
  nextPagePath: string
  previousPagePath: string
  pathPrefix: string
}

interface PageContext {
  pagination: Pagination
  categories: string[]
  slug: string
  category?: string
  next: {
    fields: {
      slug: string
      title: string
    }
  }
  prev: {
    fields: {
      slug: string
      title: string
    }
  }
}

interface Comment {
  id: string
  name: string
  email: string
  message: string
  slug: string
  date: number
}

type Children = React.ReactNode | React.ReactNode[]
