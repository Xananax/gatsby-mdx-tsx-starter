import { css } from '@emotion/react'
import { dimensions } from '../styles'

interface Props {
  className?: string
  maxWidth?: number | string
  children: React.ReactNode | React.ReactNode[]
}

export const Wrapper = ({
  children,
  className,
  maxWidth = dimensions.maxWidth
}: Props) => {
  return (
    <section
      className={className}
      css={css`
        max-width: ${maxWidth};
        margin: 0 auto;
      `}
    >
      {children}
    </section>
  )
}
