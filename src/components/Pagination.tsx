import { css } from '@emotion/react'
import { Link } from './Link'
import { View } from './mdx/View'
import { Capsule } from './mdx/Capsule'

const PaginationLink = ({ text, to }: { text: string; to: string }) =>
  to && (
    <Link to={to}>
      <Capsule>{text}</Capsule>
    </Link>
  )

export const Pagination = ({ nextPagePath, previousPagePath }: Pagination) =>
  (nextPagePath || previousPagePath) && (
    <View>
      <PaginationLink text='next page' to={nextPagePath} />
      <PaginationLink text='previous page' to={previousPagePath} />
    </View>
  )
