import { Fragment } from 'react'
import { Comment as CommentItem } from './Comment'
import { CommentForm } from './CommentForm'

interface CommentsList {
  comments: {
    node: Comment
  }[]
}

interface Props {
  url: string
  slug: string
  comments?: {
    edges: { node: Comment }[]
  }
}

export const CommentList = ({ comments }: CommentsList) => {
  return (
    <div>
      <h1>
        {comments && comments.length
          ? comments.length > 1
            ? `${comments.length} comments`
            : `1 comment`
          : 'No comments yet'}
      </h1>
      {comments &&
        comments.map(({ node: { id, ...rest } }) => (
          <CommentItem {...rest} id={id} key={id} />
        ))}
    </div>
  )
}

export const CommentListEmpty = (): null => null

export const MaybeCommentList = ({ url, slug, comments }: Props) => {
  return (
    <Fragment>
      {comments && comments.edges && comments.edges.length ? (
        <CommentList comments={comments.edges} />
      ) : (
        <CommentListEmpty />
      )}
      <CommentForm url={url} slug={slug} />
    </Fragment>
  )
}
