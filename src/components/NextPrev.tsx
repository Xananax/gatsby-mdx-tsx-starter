import { Link } from './Link'

interface Props {
  next: PageContext['next']
  prev: PageContext['prev']
}

export const NextPrev = ({ next, prev }: Props) => (
  <>
    {prev && (
      <span>
        Previous <Link to={prev.fields.slug}>{prev.fields.title}</Link>
      </span>
    )}
    {next && (
      <span>
        Next <Link to={next.fields.slug}>{next.fields.title}</Link>
      </span>
    )}
  </>
)
