import { useEffect } from 'react'
import ReactModal from 'react-modal'
import { css } from '@emotion/react'
import { lighten } from 'polished'

interface Props {
  contentLabel: string
  children: React.ReactNode[] | React.ReactNode
  isOpen: boolean
  isSmall?: boolean
  onRequestClose: () => void
}

export const Modal = ({
  children,
  contentLabel,
  isOpen,
  isSmall,
  onRequestClose
}: Props) => {
  useEffect(() => ReactModal.setAppElement('#___gatsby'), [])
  return (
    <ReactModal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      contentLabel={contentLabel}
      className='modal'
      css={
        isSmall
          ? css`
              min-width: 500px;
              min-height: 300px;
            `
          : css`
              top: 40px;
              left: 40px;
              right: 40px;
              bottom: 40px;
            `
      }
      overlayClassName='overlay'
      closeTimeoutMS={300}
    >
      <div
        css={css`
          position: absolute;
          top: 1em;
          left: 1em;
          right: 1em;
          bottom: 1em;
          display: flex;
          justify-content: center;
          align-items: center;
          overflow: hidden;
        `}
      >
        {children}
      </div>
      <a
        tabIndex={0}
        css={css`
          position: absolute;
          top: 0;
          right: 0;
          font-size: 3em;
          cursor: pointer;
          text-align: center;
          line-height: 1em;
          width: 1em;
          height: 1em;
          background: #fdfdfd;
          &:hover {
            color: ${lighten(0.3, `#25867e`)};
          }
        `}
        onClick={onRequestClose}
        onKeyUp={({ key }) => key === 'Enter' && onRequestClose()}
      >
        ×
      </a>
    </ReactModal>
  )
}
