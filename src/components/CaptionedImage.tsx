import GatsbyImage from 'gatsby-image'
import { css } from '@emotion/react'
// eslint-disable-next-line import/default
import BackgroundImage from 'gatsby-background-image'

export const alt = ({
  fields: {
    title,
    author: { name, surname }
  }
}: GatsbyImageProps) => `${title} by ${name} ${surname}`

type Props<ImageType = { childImageSharp: GatsbyImageProps }> = {
  image: ImageType
  onClick?: (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void
  href?: string
  className?: string
  imageClassName?: string
  contain?: true
  cover?: true
  isBackground?: true
}

export const MaybeCaptionedImage = ({
  image,
  href,
  className,
  imageClassName,
  cover,
  contain,
  isBackground,
  onClick,
  ...rest
}: Props) => {
  const hasWrapper = href || onClick
  isBackground = isBackground || cover || contain
  imageClassName = [imageClassName, hasWrapper ? false : className]
    .filter(Boolean)
    .join(' ')
  const Element = isBackground ? BackgroundImage : GatsbyImage
  const imageElement =
    image && 'fixed' in image.childImageSharp ? (
      <Element
        className={imageClassName}
        css={
          (cover || contain) &&
          css`
            width: 100%;
            height: 100%;
          `
        }
        fixed={image.childImageSharp.fixed}
        alt={alt(image.childImageSharp)}
        {...rest}
      />
    ) : image && 'fluid' in image.childImageSharp ? (
      <Element
        className={imageClassName}
        css={
          (cover || contain) &&
          css`
            width: 100%;
            height: 100%;
          `
        }
        fluid={image.childImageSharp.fluid}
        alt={alt(image.childImageSharp)}
        {...rest}
      />
    ) : null
  return imageElement && hasWrapper ? (
    <a
      href={href}
      onClick={onClick}
      css={css`
        display: block;
      `}
      className={className}
    >
      {imageElement}
    </a>
  ) : (
    imageElement
  )
}

export const CaptionedImageFluid = ({
  image,
  ...rest
}: Props<GatsbyImagePropsFluid>) => {
  return <MaybeCaptionedImage image={{ childImageSharp: image }} {...rest} />
}

export const CaptionedImageFixed = ({
  image,
  ...rest
}: Props<GatsbyImagePropsFixed>) => {
  return <MaybeCaptionedImage image={{ childImageSharp: image }} {...rest} />
}
