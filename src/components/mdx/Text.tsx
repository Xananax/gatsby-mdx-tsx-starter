import styled from '@emotion/styled'

interface Props {
  annotation?: boolean
}

export const Text = styled.span<Props>`
  font-size: ${({ annotation }) => (annotation ? `.7em` : `1em`)};
`
