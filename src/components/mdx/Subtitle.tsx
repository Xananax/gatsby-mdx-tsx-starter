import { forwardRef } from 'react'
import { Title, TitleProps } from './Title'

export type subTitleProps = Omit<TitleProps, 'level'>

export const Subtitle = forwardRef<HTMLElement, subTitleProps>((props, ref) => {
  return <Title level={2} {...props} ref={ref} />
})
