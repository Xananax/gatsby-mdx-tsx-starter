import styled from '@emotion/styled'

export const Capsule = styled.span`
  display: inline-block;
  padding: 0.2em 0.6em;
  font-size: 0.7em;
  position: relative;
  background-color: #262626;
  color: #fdfdfd;
  &:hover {
    background-color: #fdfdfd;
    color: #262626;
  }
`
