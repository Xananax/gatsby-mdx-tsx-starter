import {
  createElement,
  forwardRef,
  DetailedHTMLProps,
  HTMLAttributes
} from 'react'

export type TitleAttributes = DetailedHTMLProps<
  HTMLAttributes<HTMLElement>,
  HTMLElement
>

export type TitleProps = TitleAttributes & {
  level?: number
}

export const Title = forwardRef<HTMLElement, TitleProps>(
  ({ level = 1, ...props }, ref) => {
    return createElement(`h${level}`, { ...props, ref })
  }
)
