# MDX Components

These components are made available inside MDX templates. They're also available to other components

If you add or remove a component, remember to update `index.ts` accordingly