import { Paragraph } from './Paragraph'
import { Subtitle } from './Subtitle'
import { Text } from './Text'
import { Title } from './Title'

export { Paragraph, Subtitle, Text, Title }

export * from './View'

import { Link } from '../Link'

type Props<T extends HTMLElement> = React.DetailedHTMLProps<
  React.HTMLAttributes<T>,
  T
>

export const MDXLayoutComponents = {
  h1: (props: Props<HTMLHeadingElement>) => <Title {...props} />,
  h2: (props: Props<HTMLHeadingElement>) => <Subtitle {...props} />,
  p: (props: Props<HTMLParagraphElement>) => <Paragraph {...props} />,
  span: (props: Props<HTMLSpanElement>) => <Text {...props} />
}

export const MDXGlobalComponents = {
  Link
}

export const MDXComponents = { ...MDXLayoutComponents, ...MDXGlobalComponents }
