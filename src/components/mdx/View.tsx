import { forwardRef, DetailedHTMLProps, HTMLAttributes } from 'react'
import { css } from '@emotion/react'

export type DivAttributes = DetailedHTMLProps<
  HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>

export type ViewProps = DivAttributes & {
  stretch?: boolean
  fill?: boolean
  wrap?: boolean
} & ({ row?: true; column?: undefined } | { row?: undefined; column?: true }) &
  (
    | {
        center?: true
        start?: undefined
        end?: undefined
      }
    | {
        center?: undefined
        start?: true
        end?: undefined
      }
    | {
        center?: undefined
        start?: undefined
        end?: true
      }
  ) &
  (
    | {
        justifyCenter?: true
        justifyStart?: undefined
        justifyEnd?: undefined
      }
    | {
        justifyCenter?: undefined
        justifyStart?: true
        justifyEnd?: undefined
      }
    | {
        justifyCenter?: undefined
        justifyStart?: undefined
        justifyEnd?: true
      }
  ) & {
    ref?: React.LegacyRef<HTMLDivElement>
    childrenMargin?: number | string | boolean
    padding?: number | string | boolean
    margin?: number | string | boolean
  }

export const View = forwardRef<HTMLDivElement, ViewProps>(
  (
    {
      column,
      childrenMargin,
      row,
      center,
      end,
      start,
      stretch,
      fill,
      padding,
      margin,
      justifyCenter,
      justifyStart,
      justifyEnd,
      wrap,
      ...props
    }: ViewProps,
    ref
  ) => {
    if (childrenMargin === true) {
      childrenMargin = '.25em'
    }
    if (padding === true) {
      padding = '.25em'
    }
    if (margin === true) {
      margin = '.25em'
    }
    return (
      <div
        ref={ref}
        css={[
          css`
            //label: view;
            display: flex;
          `,
          wrap
            ? css`
                flex-wrap: wrap;
              `
            : css`
                flex-wrap: nowrap;
              `,
          fill &&
            css`
              width: 100%;
            `,
          stretch &&
            css`
              justify-content: space-between;
              align-items: center;
            `,
          row
            ? css`
                flex-direction: row;
              `
            : column
            ? css`
                flex-direction: column;
              `
            : false,
          center
            ? css`
                align-items: center;
              `
            : end
            ? css`
                align-items: flex-end;
              `
            : start
            ? css`
                align-items: flex-start;
              `
            : false,
          justifyCenter
            ? css`
                justify-content: center;
              `
            : justifyEnd
            ? css`
                justify-content: flex-end;
              `
            : justifyStart
            ? css`
                justify-content: flex-start;
              `
            : false,
          childrenMargin
            ? column
              ? css`
                  & > * {
                    margin-bottom: ${childrenMargin};
                    &:last-child {
                      margin-bottom: 0;
                    }
                  }
                `
              : css`
                  & > * {
                    margin-right: ${childrenMargin};
                    &:last-child {
                      margin-right: 0;
                    }
                  }
                `
            : false,
          margin &&
            css`
              margin: ${margin};
            `,
          padding &&
            css`
              padding: ${padding};
            `
        ]}
        {...props}
      />
    )
  }
)
