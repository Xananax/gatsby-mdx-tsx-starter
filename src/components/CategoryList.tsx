import { Link } from './Link'
import { View, ViewProps } from './mdx/View'
import { Capsule } from './mdx/Capsule'
import { Text } from './mdx/Text'
//import { css } from '@emotion/react'

const CategoryLink = Capsule.withComponent(Link)

type Props = Omit<ViewProps, 'ref'> & {
  base?: string
  categories: string[]
  children?: Children
  title?: string
  categoryClassName?: string
}

export const CategoryList = ({
  base = '',
  categories,
  title,
  children,
  className,
  categoryClassName,
  ...rest
}: Props) => (
  <View className={'nav' + className ? ` ${className}` : ''} {...rest}>
    {title && <Text annotation>{title}</Text>}
    {categories.map((category) => (
      <CategoryLink
        className={categoryClassName}
        key={category}
        to={`${base}${category}`}
      >
        {category}
      </CategoryLink>
    ))}
    {children}
  </View>
)
