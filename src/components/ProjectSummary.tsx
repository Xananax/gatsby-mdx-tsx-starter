import { css } from '@emotion/react'
import { MaybeCaptionedImage } from './CaptionedImage'
import { CategoryList } from './CategoryList'
import { Link } from './Link'
import { View } from './mdx/View'
import { Title } from './mdx/Title'
import { Paragraph } from './mdx/Paragraph'
import { colors } from '../styles'
import { getUrlHead } from '../utils'

export const ProjectSummary = ({
  //excerpt,
  fields: { slug, categories },
  frontmatter: { title, banner, description },
  location
}: MDXDoc<GatsbyImageProps> & { location: Location }) => {
  return (
    <View
      column
      css={css`
        position: relative;
        width: 20em;
        border-top: 2px solid ${colors.nav_link_inactive};
        border-bottom: 2px solid ${colors.nav_link_inactive};
        color: ${colors.nav_link_active};
      `}
    >
      <Link to={slug}>
        <Title
          level={3}
          css={css`
            position: relative;
            margin-top: 0.5em;
            padding-left: 1.5em;
            font-weight: 300;
            color: ${colors.nav_link_inactive};
            &::before {
              content: '';
              position: absolute;
              width: 1em;
              height: 1em;
              left: 0;
              background-color: ${colors.nav_link_active};
            }
          `}
        >
          {title}
        </Title>
      </Link>
      <View
        css={css`
          width: 100%;
          padding-bottom: 100%;
          position: relative;
          overflow: hidden;
          background-color: ${colors.nav_link_active};
        `}
      >
        <Link
          to={slug}
          css={css`
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
          `}
        >
          <MaybeCaptionedImage
            image={banner}
            cover
            css={css`
              width: 100%;
              height: 100%;
            `}
          />
        </Link>
      </View>

      <CategoryList
        className='categories'
        childrenMargin
        padding
        center
        justifyStart
        categoryClassName='category'
        base={`/${getUrlHead(location.pathname)}/categories/`}
        categories={categories}
      />
      <Link to={slug}>
        <Paragraph
          css={css`
            position: relative;
            text-align: justify;
            text-justify: inter-word;
            text-overflow: ellipsis;
            color: ${colors.nav_link_active};
            height: 7em;
            overflow: hidden;
            &::after {
              content: '';
              text-align: right;
              position: absolute;
              bottom: 0;
              right: 0;
              width: 10%;
              height: 2em;
              background: linear-gradient(
                to right,
                rgba(255, 255, 255, 0),
                rgba(255, 255, 255, 1) 50%
              );
            }
          `}
        >
          {description}
        </Paragraph>
      </Link>
    </View>
  )
}
