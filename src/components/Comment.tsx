import format from 'date-fns/format'

const formatDate = (date: number) => format(new Date(date * 1000), 'dd/MM/yyyy')

export const Comment = ({ date, email: _email, message, name }: Comment) => (
  <div>
    <p>
      <span>{name}</span>
      <span>{formatDate(date)}</span>
    </p>
    <p>{message}</p>
  </div>
)
