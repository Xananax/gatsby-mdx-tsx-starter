import { MDXProvider } from '@mdx-js/react'
import { Global } from '@emotion/react'
import { ToastContainer } from 'react-toastify'
import { MDXComponents } from './mdx'
import { PageHead } from './PageHead'
import { SEO } from './SEO'
import { styles } from '../styles'

interface Props {
  title?: string
  frontmatter?: Partial<FrontMatter<any>>
  children: React.ReactNode | React.ReactNode[]
}

export const Layout = ({ title, frontmatter = {}, children }: Props) => {
  return (
    <>
      <SEO title={title} {...frontmatter} />

      <Global styles={styles} />

      <>
        <PageHead title={title} />
        <MDXProvider components={MDXComponents}>{children}</MDXProvider>
      </>
      <ToastContainer />
    </>
  )
}
