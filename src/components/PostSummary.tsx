import { css } from '@emotion/react'
import { MaybeCaptionedImage } from './CaptionedImage'
import { CategoryList } from './CategoryList'
import { Link } from './Link'
import { View } from './mdx/View'
import { Title } from './mdx/Title'
import { Paragraph } from './mdx/Paragraph'
import { navigationLink, colors } from '../styles'
import { getUrlHead } from '../utils'

export const PostSummary = ({
  excerpt,
  fields: { slug, categories },
  frontmatter: { title, banner, date },
  location
}: MDXDoc<GatsbyImageProps> & { location: Location }) => {
  return (
    <View
      css={[
        navigationLink,
        css`
          position: relative;
          display: flex;
          border-top: 1px solid black;
          flex-direction: row;
          align-items: center;
          height: 5em;
          & .image {
            opacity: 0.3;
            transition: 1s ease-in;
          }
          &:hover .image {
            transition: 0.4s ease-in;
            opacity: 1;
          }
          &:last-of-type {
            border-bottom: 1px solid black;
          }
        `
      ]}
    >
      <Link
        to={slug}
        css={css`
          display: block;
          background-color: #222;
          position: relative;
          overflow: hidden;
          width: 5em;
          position: absolute;
          height: 100%;
        `}
      >
        <MaybeCaptionedImage cover className='image' image={banner} />
      </Link>
      <View
        column
        fill
        css={css`
          box-sizing: border-box;
          padding-left: 5.5em;
        `}
      >
        <View fill row center>
          <Link to={slug}>
            <Title
              level={2}
              css={css`
                margin: 0;
                flex-grow: 0;
              `}
            >
              {title}
            </Title>
          </Link>
          <CategoryList
            className='categories'
            childrenMargin
            padding
            center
            justifyStart
            css={css`
              flex-grow: 2;
              margin-left: 1em;
              font-size: 0.9em;
              display: none;
              @media (min-width: 50rem) {
                display: flex;
              }
              & .category {
                background: ${colors.nav_link_inactive};
                &:hover {
                  background: transparent;
                }
              }
            `}
            categoryClassName='category'
            base={`/${getUrlHead(location.pathname)}/categories/`}
            categories={categories}
          />
          <Link to={slug}>
            <Paragraph
              css={css`
                margin: 0;
                margin-right: 1em;
              `}
            >
              {date}
            </Paragraph>
          </Link>
        </View>
        <Link to={slug}>
          <Paragraph
            css={css`
              white-space: nowrap;
              overflow: hidden;
              text-overflow: ellipsis;
              margin: 0;
            `}
          >
            {excerpt}
          </Paragraph>
        </Link>
      </View>
    </View>
  )
}
