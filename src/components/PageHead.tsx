import { css } from '@emotion/react'
import { useStaticQuery, graphql } from 'gatsby'
import { MainNavigation } from './MainNavigation'
import { mediaQueryColumnToRow } from '../styles'
import { Title } from './mdx/Title'

export const PageHead = ({ title = 'my site' }) => {
  const {
    site: {
      siteMetadata: { title: site_title }
    }
  } = useStaticQuery<{ site: SiteQueryResult }>(
    graphql`
      query {
        site {
          ...site
        }
      }
    `
  )
  return (
    <header
      css={css`
        padding: 1em;
        margin: 1em;
        border-top: 0.2em solid black;
        ${mediaQueryColumnToRow('50rem')};
        justify-content: space-between;
      `}
    >
      <Title
        css={css`
          display: inline;
          margin: 0;
          padding: 0;
          line-height: 1;
          text-transform: uppercase;
          font-weight: 700;
        `}
      >
        {site_title}/{title}
      </Title>
      <MainNavigation />
    </header>
  )
}
