import { css } from '@emotion/react'
import { Link } from './Link'
import { mediaQueryColumnToRow, navigationLink } from '../styles'
import NAVIGATION from '../data/navigation'
import { colors } from '../styles/theme'

export const MainNavigation = () => {
  return (
    <nav css={mediaQueryColumnToRow('50rem')}>
      {NAVIGATION.map(({ label, partiallyActive, to }) => (
        <Link
          key={label}
          css={[
            css`
              position: relative;
              height: 1em;
              display: inline-block;
              line-height: 1;
              padding-left: 1.2em;
              &::before {
                transition: background-color 1s ease-out;
                background-color: ${colors.nav_link_inactive};
                content: '';
                position: absolute;
                width: 0.7em;
                height: 0.7em;
                left: 0;
                top: 0.15em;
                border-radius: 50%;
              }
              &.active,
              &:hover {
                &::before {
                  background-color: ${colors.nav_link_active};
                  transition: background-color 0.4s ease-out;
                }
              }
            `,
            navigationLink
          ]}
          activeClassName='active'
          partiallyActive={partiallyActive}
          to={to}
        >
          {label}
        </Link>
      ))}
    </nav>
  )
}
