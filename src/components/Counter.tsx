import { useState } from 'react'

interface Props {
  initialCounter: number
}

export const Counter = ({ initialCounter = 0 }: Props) => {
  const [counter, setCounter] = useState(initialCounter)

  const onIncrement = () => {
    setCounter((c) => c + 1)
  }

  const onDecrement = () => {
    setCounter((c) => c - 1)
  }

  return (
    <div>
      {counter}
      <div>
        <button onClick={onIncrement} type='button'>
          Increment
        </button>
        <button onClick={onDecrement} type='button'>
          Decrement
        </button>
      </div>
    </div>
  )
}
