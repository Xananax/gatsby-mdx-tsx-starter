import { Helmet } from 'react-helmet'
import { useStaticQuery, graphql } from 'gatsby'
//import default_image from '../../static/images/logo.svg'

const key = (item: Meta) => ('name' in item ? item.name : item.property)
const default_image = ''

const removeDoubles = (arr: Meta[]) => {
  const index = {} as Record<string, boolean>
  const ret = []
  const { length } = arr
  let i = 0
  while (i < length) {
    const item = arr[i++]
    if (!item) {
      continue
    }
    const name = key(item)
    if (index[name]) {
      continue
    }
    index[name] = true
    ret.push(item)
  }
  return ret
}

const filterUnique = <T extends unknown>(v: T, i: number, a: T[]) =>
  a.indexOf(v) === i

type Meta =
  | {
      property: string
      content: string
    }
  | {
      name: string
      content: string
    }

interface Props {
  description: string
  keywords: string[]
  categories: string[]
  image: string
  lang: string
  meta: Meta[]
  title: string
  children: React.ReactNode[]
}

export const SEO = ({
  description = '',
  image,
  lang = 'en',
  meta = [],
  title: page_title,
  children,
  keywords,
  categories
}: Partial<Props>) => {
  const {
    site: { siteMetadata }
  } = useStaticQuery<{ site: SiteQueryResult }>(
    graphql`
      query {
        site {
          ...site
        }
      }
    `
  )

  const metaDescription = description || siteMetadata.description
  const metaImage = image || default_image
  const title = page_title
    ? `${page_title} | ${siteMetadata.title}`
    : siteMetadata.title
  const metaKeywords = [
    ...(keywords || []),
    ...(categories || []),
    ...siteMetadata.keywords
  ]
    .filter(filterUnique)
    .join(', ')

  const _meta = removeDoubles(
    [
      {
        name: `description`,
        content: metaDescription
      },
      {
        name: `version`,
        content: siteMetadata.version
      },
      { name: 'keywords', content: metaKeywords },
      {
        property: `og:title`,
        content: title
      },
      {
        property: `og:description`,
        content: metaDescription
      },
      {
        property: `og:image`,
        content: metaImage
      },
      {
        property: `og:type`,
        content: `website`
      },
      {
        name: `twitter:image`,
        content: metaImage
      },
      {
        name: `twitter:card`,
        content: `summary`
      },
      {
        name: `twitter:creator`,
        content: siteMetadata.author.name
      },
      {
        name: `twitter:title`,
        content: title
      },
      {
        name: `twitter:description`,
        content: metaDescription
      }
    ].concat(meta)
  )

  return (
    <Helmet
      htmlAttributes={{
        lang
      }}
      title={title}
      meta={_meta}
    >
      {children}
    </Helmet>
  )
}
