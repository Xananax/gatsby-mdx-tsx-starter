import { useState } from 'react'
import { css } from '@emotion/react'
import { Modal } from './Modal'
import { CaptionedImageFluid } from './CaptionedImage'

interface ImageGridProps {
  edges: {
    node: {
      childImageSharp: GatsbyImagePropsFluid
    }
  }[]
  limit?: number
}

const ImageGridImage = ({
  image,
  onSelect
}: {
  image: GatsbyImagePropsFluid
  onSelect: () => void
}) => {
  const aspect =
    image.fluid.aspectRatio > 1
      ? css`
          grid-column: span 2;
          grid-row: span 1;
        `
      : image.fluid.aspectRatio < 1
      ? css`
          grid-column: span 1;
          grid-row: span 2;
        `
      : css`
          grid-column: span 1;
          grid-row: span 1;
        `
  return (
    <a
      tabIndex={0}
      css={css`
        cursor: pointer;
        background: blue;
        ${aspect}
        overflow: hidden;
        height: auto;
        box-sizing: border-box;
        margin: 0.5%;
        display: inline-block;
        transition: transform 0.3s ease-out;
        z-index: 0;
        outline: none;
        &:hover,
        &:focus {
          transition: transform 0.2s cubic-bezier(0.75, -0.5, 0, 1.75);
          transform: scale(1.1);
          z-index: 1;
        }
        &:focus {
          box-shadow: 0 0 0.2em rgba(0, 0, 0, 0.4);
        }
      `}
      onClick={onSelect}
      onKeyUp={({ key }) => key === 'Enter' && onSelect()}
    >
      <CaptionedImageFluid image={image} cover />
    </a>
  )
}

export const ImageGrid = ({ limit = 0, edges }: ImageGridProps) => {
  const images = (limit ? edges.slice(0, limit) : edges).map(
    ({ node: { childImageSharp } }) => childImageSharp
  )
  const [selectedImage, setSelectedImage] = useState(-1)

  const closeModal = () => setSelectedImage(-1)
  const isOpen = selectedImage > -1

  const currentImage = images[selectedImage]

  return (
    <div
      css={css`
        width: 100%;
        text-align: center;
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(20em, 1fr));
        grid-template-rows: repeat(6, 20em);
        grid-gap: 1rem;
        grid-auto-flow: dense;
      `}
    >
      <Modal isOpen={isOpen} onRequestClose={closeModal} contentLabel='Image'>
        {currentImage && (
          <CaptionedImageFluid
            key={currentImage.id}
            image={currentImage}
            css={css`
              width: 100%;
              height: 100%;
              height: auto;
              box-sizing: border-box;
              display: inline-block;
            `}
          />
        )}
      </Modal>
      {images.map((image, index) => (
        <ImageGridImage
          key={image.id}
          image={image}
          onSelect={() => setSelectedImage(index)}
        />
      ))}
      {images.map((image, index) => (
        <ImageGridImage
          key={image.id}
          image={image}
          onSelect={() => setSelectedImage(index)}
        />
      ))}
    </div>
  )
}
