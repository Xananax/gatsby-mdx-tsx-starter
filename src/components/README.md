# Components

Here are all the presentational components of the site.

If anything needs styling, it will be here and nowhere else _except_ for:

- Stand alone pages in `pages`
- Templates in `templates`

If you add or remove a component, remember to update `index.ts` accordingly