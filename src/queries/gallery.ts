import { graphql } from 'gatsby'

export const fragment = graphql`
  fragment gallery on Query {
    allFile(
      sort: { fields: [base], order: ASC }
      filter: {
        extension: { regex: "/(jpg)|(png)|(jpeg)/" }
        sourceInstanceName: { eq: "images" }
      }
    ) {
      edges {
        node {
          base
          ...galleryImage
        }
      }
    }
  }
`
