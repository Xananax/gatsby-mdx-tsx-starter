import { graphql } from 'gatsby'

export const fragment = graphql`
  fragment image on File {
    childImageSharp {
      fluid(maxWidth: 900) {
        ...GatsbyImageSharpFluid
      }
      fields {
        album
        author {
          name
          surname
        }
        title
      }
    }
  }
`
