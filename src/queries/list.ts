import { graphql } from 'gatsby'

export const fragment = graphql`
  fragment mdxlist on MdxConnection {
    edges {
      node {
        excerpt(pruneLength: 300)
        id
        fields {
          slug
          categories
          keywords
        }
        ...MdxFrontmatter
      }
    }
  }
`
