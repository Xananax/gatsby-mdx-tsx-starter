import { graphql } from 'gatsby'

export const fragment = graphql`
  fragment mdx on Mdx {
    fields {
      slug
      categories
      keywords
    }
    ...MdxFrontmatter
    body
  }
`
