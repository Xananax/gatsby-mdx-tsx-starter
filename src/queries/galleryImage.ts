import { graphql } from 'gatsby'

export const fragment = graphql`
  fragment galleryImage on File {
    childImageSharp {
      id
      fluid(maxWidth: 1080, quality: 90) {
        ...GatsbyImageSharpFluid
      }
      fields {
        album
        author {
          name
          surname
        }
        title
      }
    }
  }
`
