import { graphql } from 'gatsby'

export const fragment = graphql`
  fragment MdxFrontmatter on Mdx {
    frontmatter {
      title
      date(formatString: "MMMM DD, YYYY")
      banner {
        ...image
      }
      description
    }
  }
`
