import { graphql } from 'gatsby'

export const fragment = graphql`
  fragment comments on CommentsYamlConnection {
    edges {
      node {
        id
        name
        email
        message
        date
      }
    }
  }
`
