import { graphql } from 'gatsby'

export const fragment = graphql`
  fragment site on Site {
    siteMetadata {
      version
      title
      siteUrl
      repository
      description
      author {
        name
        url
        email
      }
      keywords
    }
  }
`
