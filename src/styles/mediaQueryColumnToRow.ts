import { css } from '@emotion/react'
import { childrenMargin, resetChildrenMargin } from './childrenMargin'

export const mediaQueryColumnToRow = (breakpoint: string | number) => [
  childrenMargin('bottom'),
  css`
    // mobile first
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    align-items: flex-start;
    // over ${breakpoint}:
    @media (min-width: ${breakpoint}) {
      align-items: center;
      flex-direction: row;
      ${resetChildrenMargin('bottom')};
      ${childrenMargin('right')};
    }
  `
]
