import { css } from '@emotion/react'
import 'react-toastify/dist/ReactToastify.css'
import 'prismjs/themes/prism-okaidia.css'

const modal = css`
  .modal {
    position: absolute;
    background-color: #fdfdfd;
    box-shadow: 0 0 1em rgba(0, 0, 0, 0.5);
    outline: none;
  }
  .overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.2);
    box-shadow: inset 0 0 3em 1em rgba(0, 0, 0, 0.3);
    z-index: 10;
    display: flex;
    justify-content: center;
    align-items: center;
    align-content: center;
  }
  .ReactModal__Overlay {
    opacity: 0;
    transition: opacity 300ms ease-in-out;
  }
  .ReactModal__Overlay--after-open {
    opacity: 1;
  }
  .ReactModal__Overlay--before-close {
    opacity: 0;
  }
`

export const styles = css`
  html,
  body {
    margin: 0;
    padding: 0;
    scroll-behavior: smooth;
  }
  @media print {
    p a[href^="http://"]:after, a[href^="https://"]:after
    {
      content: ' (' attr(href) ')';
    }
    p a {
      word-wrap: break-word;
    }
    p {
      page-break-inside: avoid;
    }
  }
  a {
    color: inherit;
  }
  pre {
    background-color: #2f1e2e !important;
    border-radius: 4px;
    font-size: 14px;
  }
  .gatsby-highlight-code-line {
    background-color: #4f424c;
    display: block;
    margin-right: -1em;
    margin-left: -1em;
    padding-right: 1em;
    padding-left: 1em;
  }
  ${modal};
`
