import Typography from 'typography'
import gray from 'gray-percentage'
import {
  TABLET_MEDIA_QUERY,
  MOBILE_MEDIA_QUERY
} from 'typography-breakpoint-constants'
import verticalRhythm from 'compass-vertical-rhythm'

// based on https://github.com/KyleAMathews/typography.js/tree/master/packages/typography-theme-irving
export default new Typography({
  baseFontSize: '17px',
  baseLineHeight: 1.82,
  scaleRatio: 2.25,
  googleFonts: [
    {
      name: 'Merriweather Sans',
      styles: ['400', '800']
    },
    {
      name: 'Merriweather',
      styles: ['400', '400i', '700']
    }
  ],
  headerFontFamily: ['Merriweather Sans', 'sans-serif'],
  bodyFontFamily: ['Merriweather', 'sans-serif'],
  headerWeight: 800,
  bodyWeight: 400,
  boldWeight: 700,
  overrideStyles: ({ adjustFontSizeTo, scale, rhythm }, options) => {
    const vr = verticalRhythm({
      baseFontSize: '15px',
      baseLineHeight: '27.35px'
    })

    return {
      a: {
        textDecoration: 'none'
      },
      'nav a, .nav a': {
        fontFamily: 'Merriweather Sans'
      },
      'h1,h2,h3,h4,h5,h6': {
        marginTop: rhythm(2)
      },
      blockquote: {
        ...scale(1 / 5),
        color: gray(41),
        paddingLeft: rhythm(18 / 16),
        marginLeft: 0,
        borderLeft: `${rhythm(6 / 16)} solid`,
        borderColor: gray(90)
      },
      'blockquote > :last-child': {
        marginBottom: 0
      },
      'blockquote cite': {
        ...adjustFontSizeTo(options.baseFontSize),
        fontWeight: options.bodyWeight
      },
      'blockquote cite:before': {
        content: '"— "'
      },
      [MOBILE_MEDIA_QUERY]: {
        blockquote: {
          marginLeft: rhythm(-3 / 4),
          marginRight: 0,
          borderLeft: `${rhythm(3 / 16)} solid`,
          borderColor: gray(90),
          paddingLeft: rhythm(9 / 16)
        }
      },
      [TABLET_MEDIA_QUERY]: {
        html: {
          ...vr.establishBaseline()
        }
      }
    }
  }
})
