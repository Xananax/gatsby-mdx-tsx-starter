export const colors = {
  nav_link_inactive: '#777',
  nav_link_active: '#000'
}

export const dimensions = {
  maxWidth: '1500px'
}
