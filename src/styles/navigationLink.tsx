import { css } from '@emotion/react'
import { colors } from './theme'

export const navigationLink = css`
  text-decoration: none;
  transition: color 1s ease-out;
  color: ${colors.nav_link_inactive};
  &.active,
  &:hover {
    transition: color 0.4s ease-out;
    color: ${colors.nav_link_active};
  }
`
