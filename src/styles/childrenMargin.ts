type Direction = 'right' | 'left' | 'top' | 'bottom'

export const childrenMargin = (
  direction: Direction,
  amount: string | number = '1em'
) => {
  amount = amount === '0' || !amount ? 0 : amount
  direction = (direction[0].toUpperCase() + direction.slice(1)) as Direction
  return {
    '& > *': {
      [`margin${direction}`]: amount,
      ...(amount
        ? {
            [`&:last-child`]: {
              [`margin${direction}`]: 0
            }
          }
        : {})
    }
  }
}

export const resetChildrenMargin = (direction: Direction) =>
  childrenMargin(direction, 0)
