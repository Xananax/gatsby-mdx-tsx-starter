export const getUrlHead = (url: string) =>
  url && url.split('/').filter(Boolean)[0]
