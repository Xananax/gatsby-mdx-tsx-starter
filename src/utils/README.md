# Utils

Various utilities to be used throughout the site.

If you add a utility, please update `index.ts` accordingly.