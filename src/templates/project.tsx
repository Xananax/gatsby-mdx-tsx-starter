import { graphql } from 'gatsby'
import {
  MaybeCaptionedImage,
  MDXRenderer,
  MaybeCommentList,
  Layout,
  NextPrev,
  CategoryList
} from '../components'
import { getUrlHead } from '../utils'

interface Props {
  data: {
    site: SiteQueryResult
    mdx: MDXDoc<GatsbyImagePropsFixed>
    allCommentsYaml: {
      edges: {
        node: Comment
      }[]
    }
  }
  pageContext: PageContext
  location: Location
}

export default function Project({
  data: {
    mdx: { body, fields, frontmatter },
    allCommentsYaml,
    site: { siteMetadata }
  },
  pageContext,
  location
}: Props) {
  const { title, date, banner } = frontmatter

  return (
    <Layout frontmatter={frontmatter}>
      <h1>{title}</h1>
      <h2>{date}</h2>

      <MaybeCaptionedImage image={banner} />

      <MDXRenderer>{body}</MDXRenderer>

      <div>
        <CategoryList
          base={`/${getUrlHead(location.pathname)}/categories/`}
          categories={fields.categories}
        />

        <hr />
        <NextPrev {...pageContext} />
      </div>
      <MaybeCommentList
        url={siteMetadata.repository}
        slug={pageContext.slug}
        comments={allCommentsYaml}
      />
    </Layout>
  )
}

export const pageQuery = graphql`
  query($id: String!, $slug: String!) {
    site {
      ...site
    }
    mdx(fields: { id: { eq: $id } }) {
      ...mdx
    }
    allCommentsYaml(filter: { slug: { eq: $slug } }) {
      ...comments
    }
  }
`
