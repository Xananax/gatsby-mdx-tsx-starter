import { graphql } from 'gatsby'
import {
  Layout,
  CategoryList,
  Pagination,
  ProjectSummary,
  View,
  Wrapper
} from '../components'
import { css } from '@emotion/react'
import { getUrlHead } from '../utils'

interface Props {
  data: {
    site: SiteQueryResult
    allMdx: MDXDocList<GatsbyImagePropsFixed>
  }
  pageContext: PageContext
  location: Location
}

export const ProjectList = ({
  data: { allMdx },
  pageContext: { pagination, categories },
  location
}: Props) => {
  const { page } = pagination

  const projects = page.map((id) =>
    allMdx.edges.find((edge) => edge.node.id === id)
  )

  console.log(projects)

  return (
    <Layout title='Project'>
      <Wrapper>
        <CategoryList
          fill
          childrenMargin
          padding
          center
          justifyCenter
          wrap
          base={`/${getUrlHead(location.pathname)}/categories/`}
          categories={categories}
        />
        <View
          row
          wrap
          justifyCenter
          css={css`
            & > * {
              margin: 3em 2em;
            }
          `}
        >
          {projects.map(({ node }) => (
            <ProjectSummary location={location} key={node.id} {...node} />
          ))}
        </View>
        <Pagination {...pagination} />
      </Wrapper>
    </Layout>
  )
}

export default ProjectList

export const pageQuery = graphql`
  query {
    allMdx {
      ...mdxlist
    }
  }
`
