import * as React from 'react'
import { graphql } from 'gatsby'
import { MDXRenderer, Layout, Title, Wrapper } from '../components'
//import Logo from '../assets/icon.svg'

interface Props {
  data: {
    mdx: MDXDoc<GatsbyImagePropsFixed>
  }
  pageContext: PageContext
}

export const Page = ({
  data: {
    mdx: { frontmatter, body }
  }
}: Props) => {
  return (
    <Layout frontmatter={frontmatter} title={frontmatter.title}>
      <Wrapper>
        <Title>{frontmatter.title}</Title>
        <MDXRenderer>{body}</MDXRenderer>
      </Wrapper>
    </Layout>
  )
}

export default Page

export const pageQuery = graphql`
  query($id: String!) {
    mdx(fields: { id: { eq: $id } }) {
      ...mdx
    }
  }
`
