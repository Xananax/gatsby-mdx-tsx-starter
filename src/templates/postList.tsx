import { graphql } from 'gatsby'
import {
  Layout,
  CategoryList,
  Pagination,
  PostSummary,
  Wrapper
} from '../components'
import { getUrlHead } from '../utils'

interface Props {
  data: {
    site: SiteQueryResult
    allMdx: MDXDocList<GatsbyImagePropsFixed>
  }
  pageContext: PageContext
  location: Location
}

export const PostList = ({
  data: { allMdx },
  pageContext: { pagination, categories },
  location
}: Props) => {
  const { page } = pagination

  const posts = page.map((id) =>
    allMdx.edges.find((edge) => edge.node.id === id)
  )

  return (
    <Layout title='Blog'>
      <Wrapper>
        <CategoryList
          fill
          childrenMargin
          padding
          center
          justifyEnd
          wrap
          base={`/${getUrlHead(location.pathname)}/categories/`}
          categories={categories}
        />
        {posts.map(({ node }) => (
          <PostSummary location={location} key={node.id} {...node} />
        ))}
        <Pagination {...pagination} />
      </Wrapper>
    </Layout>
  )
}

export default PostList

export const pageQuery = graphql`
  query {
    allMdx {
      ...mdxlist
    }
  }
`
