import { graphql } from 'gatsby'
import { Wrapper, Layout } from '../components'

export const PageIndex = () => {
  return (
    <Layout title='home'>
      <Wrapper>
        <h1>Welcome to Site</h1>
      </Wrapper>
    </Layout>
  )
}

export default PageIndex

export const pageQuery = graphql`
  query {
    site {
      ...site
    }
  }
`
