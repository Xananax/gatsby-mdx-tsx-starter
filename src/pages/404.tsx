import { Wrapper, Layout } from '../components'

export const Page404 = () => (
  <Layout title='404 not found'>
    <Wrapper>
      <h1>NOT FOUND</h1>
      <p>You just hit a route that doesn't exist... the sadness.</p>
    </Wrapper>
  </Layout>
)

export default Page404
