import { useStaticQuery, graphql } from 'gatsby'
import { Layout, Wrapper, ImageGrid } from '../components'

type UrlProps = {
  allFile: {
    edges: {
      node: {
        childImageSharp: GatsbyImagePropsFluid
      }
    }[]
  }
}

export const PageGallery = () => {
  const {
    allFile: { edges }
  } = useStaticQuery<UrlProps>(
    graphql`
      query {
        ...gallery
      }
    `
  )
  return (
    <Layout title='gallery'>
      <Wrapper>
        <ImageGrid edges={edges} />
      </Wrapper>
    </Layout>
  )
}

export default PageGallery
