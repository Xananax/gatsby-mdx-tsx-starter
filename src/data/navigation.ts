export default [
  { to: '/', label: 'Home' },
  { to: '/about', label: 'About' },
  { to: '/blog', label: 'Blog', partiallyActive: true },
  { to: '/gallery', label: 'Gallery' },
  { to: '/projects', label: 'Projects' }
]
